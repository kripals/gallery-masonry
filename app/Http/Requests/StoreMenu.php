<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMenu extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required'
        ];
    }
//    public function subMenuFillData($menu)
//    {
//        if ($this->get('page'))
//        {
//            $page  = Page::find($this->get('page'));
//            $route = route('page.show', $page->slug);
//            $title = $page->title;
//        }
//        elseif ( ! empty($this->custom_url))
//        {
//            $route = $this->custom_url;
//        }
//        else
//        {
//            $title = "Title Page";
//            $route = 'javascript:void(0);';
//        }
//
//
//
//        $inputs = [
//            'menu_id' => $menu->id,
//            'name'    => $this->has('name') ? $this->get('name') : $title,
//            'url'     => $route,
//        ];
//
//        return $inputs;
//
//    }

//    public function childsubMenuFillData($subMenu)
//    {
//        if ($this->get('page'))
//        {
//            $page  = Page::find($this->get('page'));
//            $route = route('page.show', $page->slug);
//            $title = $page->title;
//        }
//        elseif ( ! empty($this->custom_url))
//        {
//            $route = $this->custom_url;
//        }
//        else
//        {
//            $title = "Title Page";
//            $route = 'javascript:void(0);';
//        }
//
//        $inputs = [
//            'sub_menu_id' => $subMenu->id,
//            'name'        => $this->has('name') ? $this->get('name') : $title,
//            'url'         => $route,
//        ];
//
//        return $inputs;


        //=======
        //        if(sizeof($this->submenu->childsubMenus) == 0 ){
        //            $first_child_sub_menu_order = 0;
        //        }
        //        else{
        //            $first_child_sub_menu_order = $this->submenu->childsubMenus->sortByDesc('order')->first()->order;
        //        }
        //        if (!is_null($first_child_sub_menu_order)) {
        //            $order = intval($first_child_sub_menu_order) +1;
        //        }
        //        $inputs = [
        //            'sub_menu_id' => $this->submenu->id,
        //            'name' => $this->has('name') ? $this->get('name') : $title,
        //            'url' => $route,
        //            'order' => $order
        //        ];
        //        return $inputs;
        //>>>>>>> 199954e04e39c5d8be2b7324ec0b98e92b57b151
//    }

    public function menuFillData()
    {
        if ($this->get('page'))
        {
            $page  = Page::find($this->get('page'));
            $route = route('pageShow', $page->slug);
            $title = $page->title;
        }
        elseif ( ! empty($this->custom_url))
        {
            $route = $this->custom_url;
        }
        else
        {
            $title = "Title Page";
            $route = "javascript:void(0);";
        }

        $inputs = [
            'name'  => $this->has('name') ? $this->get('name') : $title,
            'url'   => $route,
            'order' => intval(Menu::orderBy('order', 'desc')->first()->order) + 1
        ];

        return $inputs;
    }
}
