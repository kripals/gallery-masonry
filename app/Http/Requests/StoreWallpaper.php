<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreWallpaper extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'name' =>'required'
        ];
    }

    public function data()
    {
//        dd($this->all());
       $data = [
           'name'                 => $this->get('name'),
           'is_published'          => $this->has('publish'),
       ];

       return $data;
    }
}
