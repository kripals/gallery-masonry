<?php

namespace App\Http\ViewCreators;

use Illuminate\View\View;

class BackendMenuCreator {

    /**
     * The user model.
     *
     * @var \App\User;
     */
    protected $user;

    /**
     * Create a new menu bar composer.
     */
    public function __construct()
    {
        $this->user = auth()->user();
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function create(View $view)
    {
        $menu[] = [
            'class' => false,
            'route' => url('backend/home'),
            'icon'  => 'md md-home',
            'title' => 'Home'
        ];

        array_push($menu, [
            'class' => false,
            'route' => route('wallpaper.index'),
            'icon'  => 'md md-list',
            'title' => 'Wallpaper'
        ]);

        array_push($menu, [
            'class' => false,
            'route' => route('collection.index'),
            'icon'  => 'md md-pages',
            'title' => 'Collection'
        ]);

        array_push($menu, [
            'class' => false,
            'route' => route('tag.index'),
            'icon'  => 'md md-web',
            'title' => 'Tag'
        ]);

        array_push($menu, [
            'class' => false,
            'route' => route('category.index'),
            'icon'  => 'md md-image',
            'title' => 'Category'
        ]);

        array_push($menu, [
            'class' => false,
            'route' => route('img.index'),
            'icon'  => 'md md-image',
            'title' => 'Tag Images'
        ]);
        $view->with('allMenu', $menu);
    }
}