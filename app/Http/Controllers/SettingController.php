<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

use App\Http\Requests;

class SettingController extends Controller
{

    public function index()
    {
        $settings = Setting::all();

        return view('backend.setting.index', compact('settings'));
    }


    public function update(Request $request)
    {
        foreach ($request->get('setting') as $slug => $value)
        {
            $setting = Setting::firstOrCreate([
                'slug'  => $slug
            ]);

            if ($request->hasFile($slug))
            {
                $imageDetails = [
                    'name'      => $request->{$slug}->getClientOriginalName(),
                    'size'      => $request->{$slug}->getSize(),
                    'path'      => request()->{$slug}->store(null, 'public')
                ];

                if ($setting->image)
                {
                    $setting->image->deleteImage();
                    $setting->image->update($imageDetails);
                }
                else
                {
                    $setting->image()->create($imageDetails);
                }

                $value = $imageDetails['path'];
            }

            if ($setting) $setting->update(['value' => $value ]);

            if ($request->hasFile('file'))
            {
                if ($request->file('file')->isValid())
                {
                    $name       ='employee_form.pdf';

                    $request->file('file')->move('documents', $name);
                }
            }
        }

        return redirect()->back()->with('success', trans('messages.update_success', ['entity' => 'Setting']));
    }
}
