<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMenu;
use App\Models\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function index()
    {
        return view('backend.menu.index');
    }

    public function store(StoreMenu $request)
    {
        $menu = Menu::create($request->menuFillData());
        dd($menu);

        return back()->with('success', 'menu has been stored', [ 'entity' => 'Menu' ])->with('collapse_in', $menu->id);
    }
}
