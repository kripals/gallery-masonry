<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreImg;
use App\Http\Requests\UpdateImg;
use App\Models\Img;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ImgController extends Controller
{
    public function index()
    {
        $img = Img::orderBy('id', 'ASC')->get();

        return view('backend.img.index', compact('img'));
    }

    public function create()
    {
        $tags = Tag::all()->pluck('name','id');

        return view('backend.img.create',compact('tags'));
    }

    public function store(StoreImg $request)
    {
        DB::transaction(function () use ($request) {
            $data = $request->data();

            $img = Img::create($data);
            foreach($request->tag as $tags){
                $pivotimgs = $img->tag()->attach($tags);
            }
            $this->uploadRequestImage($request, $img);
        });

        return redirect()->route('img.index')->withSuccess(trans('Img has been added successfully', [ 'entity' => 'Img' ]));
    }

    public function edit(Img $img)
    {
        return view('backend.img.edit', compact('img'));
    }

    public function update(UpdateImg $request, Img $img)
    {
        DB::transaction(function () use ($request, $img) {

            $data = $request->data();

            $img->update($data);

            if ($request->image)
            {
                $this->uploadRequestImage($request, $img);
            }
        });

        return redirect()->route('img.index')->with('success', 'Image has been updated', ['entity'=>'Img']);
    }


    public function destroy(Img $img)
    {
        $img->delete();

        return back()->with('success', 'Image has been deleted');
    }
}
