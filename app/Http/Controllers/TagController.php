<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTag;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TagController extends Controller
{
    public function index()
    {
        $tag = Tag::orderBy('id', 'ASC')->get();

        return view('backend.tag.index', compact('tag'));
    }

    public function store(StoreTag $request)
    {
        DB::transaction(function () use ($request) {

            $data = $request->data();

            $tag = Tag::create($data);

            $this->uploadRequestImage($request, $tag);
//            dd($request->all());

        });

        return redirect()->route('tag.index')->withSuccess(trans('Tag has been added successfully', [ 'entity' => 'Tag' ]));
    }

    public function edit(Tag $tag)
    {
        return view('backend.tag.edit', compact('tag'));
    }

    public function update(UpdateTag $request, Tag $tag)
    {
        DB::transaction(function () use ($request, $tag) {

            $data = $request->data();

            $tag->update($data);

            if ($request->image)
            {
                $this->uploadRequestImage($request, $tag);
            }
        });



        return redirect()->route('tag.index')->with('success', 'Tag has been updated', ['entity'=>'Tag']);
    }


    public function destroy(Tag $tag)
    {
        $tag->delete();

        return back()->with('success', 'wallpaper has been deleted');
    }
}
