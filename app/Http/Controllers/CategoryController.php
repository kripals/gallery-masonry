<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCategory;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function index()
    {
        $category = Category::orderBy('id', 'ASC')->get();

        return view('backend.category.index', compact('category'));
    }

    public function store(StoreCategory $request)
    {
        DB::transaction(function () use ($request) {

            $data = $request->data();

            $category = Category::create($data);

            $this->uploadRequestImage($request, $category);
        });

        return redirect()->route('category.index')->withSuccess(trans('Category has been added successfully', [ 'entity' => 'Category' ]));
    }

    public function edit(Category $category)
    {
        return view('backend.category.edit', compact('category'));
    }

    public function update(UpdateCategory $request, Category $category)
    {
        DB::transaction(function () use ($request, $category) {

            $data = $request->data();

            $category->update($data);

            if ($request->image)
            {
                $this->uploadRequestImage($request, $category);
            }
        });


        return redirect()->route('category.index')->with('success', 'Category has been updated', [ 'entity' => 'Category' ]);
    }


    public function destroy(Category $category)
    {
        $category->delete();

        return back()->with('success', 'category has been deleted');
    }
}
