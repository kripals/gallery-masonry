<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function homepage()
    {
    return view('frontend.homepage');
    }

    public function collections()
    {
        return view('frontend.collections');
    }

    public function wallpapers()
    {
        return view('frontend.wallpapers');
    }
}
