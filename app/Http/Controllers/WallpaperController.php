<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreWallpaper;
use App\Http\Requests\UpdateWallpaper;
use App\Models\Category;
use App\Models\Wallpaper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WallpaperController extends Controller
{
    public function index()
    {
        $wallpaper = Wallpaper::orderBy('id', 'ASC')->get();

        return view('backend.wallpaper.index', compact('wallpaper'));
    }

    public function create()
    {
        $categories = Category::all()->pluck('name','id');

        return view('backend.wallpaper.create',compact('categories'));
    }

    public function store(StoreWallpaper $request)
    {
        DB::transaction(function () use ($request) {
            $data = $request->data();

            $wallpaper = Wallpaper::create($data);
            foreach($request->category as $cat){
               $pivot = $wallpaper->category()->attach($cat);
            }
            $this->uploadRequestImage($request, $wallpaper);
        });

        return redirect()->route('wallpaper.index')->withSuccess(trans('Wallpaper has been added successfully', [ 'entity' => 'Wallpaper' ]));
    }

    public function edit(Wallpaper $wallpaper)
    {
        return view('backend.wallpaper.edit', compact('wallpaper'));
    }

    public function update(UpdateWallpaper $request, Wallpaper $wallpaper)
    {
        DB::transaction(function () use ($request, $wallpaper) {

            $data = $request->data();

            $wallpaper->update($data);

            if ($request->image)
            {
                $this->uploadRequestImage($request, $wallpaper);
            }
        });

        return redirect()->route('wallpaper.index')->with('success', 'Wallpaper has been updated', ['entity'=>'Wallpaper']);
    }


    public function destroy(Wallpaper $wallpaper)
    {
        $wallpaper->delete();

        return back()->with('success', 'wallpaper has been deleted');
    }
}
