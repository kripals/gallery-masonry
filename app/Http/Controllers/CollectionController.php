<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCollection;
use App\Http\Requests\UpdateCollection;
use App\Models\Collection;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CollectionController extends Controller
{
    public function index()
    {
        $collection = Collection::orderBy('id', 'ASC')->get();

        return view('backend.collection.index', compact('collection'));
    }

    public function create()
    {
        $tags = Tag::all()->pluck('name','id');

        return view('backend.collection.create', compact('tags'));
    }

    public function store(StoreCollection $request)
    {
        DB::transaction(function () use ($request) {
            $data = $request->data();

            $collection = Collection::create($data);

            foreach($request->tag as $tags){
                $pivotcollection = $collection->tag()->attach($tags);
            }

            $this->uploadRequestImage($request, $collection);
//
        });

        return redirect()->route('collection.index')->withSuccess(trans('Collection has been added successfully', [ 'entity' => 'Collection' ]));
    }

    public function edit(Collection $collection)
    {
        return view('backend.collection.edit', compact('collection'));
    }

    public function update(UpdateCollection $request, Collection $collection)
    {
        DB::transaction(function () use ($request, $collection) {

            $data = $request->data();

            $collection->update($data);

            if ($request->image)
            {
                $this->uploadRequestImage($request, $collection);
            }
        });



        return redirect()->route('collection.index')->with('success', 'Collection has been updated', ['entity'=>'Collection']);
    }


    public function destroy(Collection $collection)
    {
        $collection->delete();

        return back()->with('success', 'wallpaper has been deleted');
    }

}
