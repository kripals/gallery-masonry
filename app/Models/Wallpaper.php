<?php

namespace App\Models;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;

class Wallpaper extends Model
{
    protected $fillable = [
        'slug',
        'name',
        'is_primary',
        'is_published',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;

        if (!$this->exists) {
            $this->setUniqueSlug($value, '');
        }
    }

    protected function setUniqueSlug($name, $extra)
    {
        $slug = str_slug($name . '_' . $extra);

        if (static::whereSlug($slug)->exists()) {

            $this->setUniqueSlug($name, intval($extra) + 1);

            return;
        }

        $this->attributes['slug'] = $slug;
    }

    public function category()
    {
        return $this->belongsToMany(Category::class);
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

}
