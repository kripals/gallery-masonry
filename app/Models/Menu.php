<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = [
        'slug',
        'name',
        'url',
        'order'
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;

        if (!$this->exists) {
            $this->setUniqueSlug($value, '');
        }
    }

    protected function setUniqueSlug($name, $extra)
    {
        $slug = str_slug($name . '_' . $extra);

        if (static::whereSlug($slug)->exists()) {
            $this->setUniqueSlug($name, $extra + 1);

            return;
        }

        $this->attributes['slug'] = $slug;
    }

    public function addablemenu()
    {
        return $this->morphTo();
    }
    //    public function image()
    //    {
    //        return $this->morphOne(Image::class, 'imageable');
    //    }

//    public function subMenus()
//    {
//        return $this->hasMany(SubMenu::class)->with('childsubMenus');
//    }
}
