<?php

namespace App\Models;

use App\Img;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
        'slug',
        'name',
        'is_published',
        'is_primary',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;

        if (!$this->exists) {
            $this->setUniqueSlug($value, '');
        }
    }

    protected function setUniqueSlug($name, $extra)
    {
        $slug = str_slug($name . '-' . $extra);

        $extra = !empty($extra) ?: 1;

        if (static::whereSlug($slug)->exists())
        {
            $this->setUniqueSlug($name, $extra + 1);

            return;
        }
        $this->attributes['slug'] = $slug;
    }

    public function collection()
    {
        return $this->belongsToMany(Collection::class);
    }

    public function img()
    {
        return $this->belongsToMany(Img::class);
    }
}
