<!-- Header -->
<header class="aurel_main_header aurel_logo_left aurel_border_off aurel_header_gradient_style aurel_sticky_menu_on">
    <div class="aurel_main_header_wrapper">
        <!-- Nav Bar Start -->
        <div class="aurel_header_left_part aurel_header_logo_part">
            <div class="aurel_logo_cont">
                <a class="aurel_image_logo aurel_dp aurel_no_select aurel_retina" href=""></a>
            </div>
        </div>

        <!-- Nav Bar Start -->
        <div class="aurel_header_middle_part aurel_header_menu_part">
            <nav class="aurel_nav">
                <ul class="aurel_menu">
                    <li class="menu-item-has-children">
                        <a href="{{ route('collections') }}">Collectons</a>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="{{ route('wallpapers') }}">Wallpapers</a>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="javascript:void(0)">Contacts</a>

                        <ul class="sub-menu">
                            <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/contacts_1.html">Contacts
                                    01</a></li>
                            <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/contacts_2.html">Contacts
                                    02</a></li>
                            <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/contacts_3.html">Contacts
                                    03</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
        <!-- Nav Bar End -->

        <!-- Social Site Start -->
        <div class="aurel_header_right_part aurel_header_socials_part">
            <div class="aurel_header_socials">
                <a class="aurel_footer_social_button aurel_facebook" href="https://www.facebook.com/"><i
                            class="fa fa-facebook"></i></a>
                <a class="aurel_footer_social_button aurel_twitter" href="https://twitter.com/"><i
                            class="fa fa-twitter"></i></a>
                <a class="aurel_footer_social_button aurel_google_plus" href="https://plus.google.com/"><i
                            class="fa fa-google-plus"></i></a>
                <a class="aurel_footer_social_button aurel_instagram" href="https://www.instagram.com/?hl=ru"><i
                            class="fa fa-instagram"></i></a>
                <a class="aurel_footer_social_button aurel_pinterest" href="https://pinterest.com/"><i
                            class="fa fa-pinterest"></i></a>
                <a class="aurel_toogle_fullview" href="javascript:void(0)">
                    <span class="aurel_fullview_block01"></span>
                    <span class="aurel_fullview_block02"></span>
                    <span class="aurel_fullview_block03"></span>
                    <span class="aurel_fullview_block04"></span>
                </a>
            </div>
        </div>
        <!-- Social Site End -->

        <!-- Logo Mobile Start -->
        <div class="aurel_header_mobile_logo">
            <div class="aurel_logo_cont">
                <a href="http://pixel-mafia.com/demo/html-templates/aurel/index.html"
                   class="aurel_image_logo aurel_dp aurel_no_select aurel_retina"></a>
            </div>
        </div>
        <!-- Logo Mobile End -->

        <!-- Menu Mobile Start -->
        <a class="aurel_header_mobile_menu_toggler" href="javascript:void(0)">
					<span class="aurel_mmt_icon">
						<span class="aurel_mmt_line01"></span>
						<span class="aurel_mmt_line02"></span>
						<span class="aurel_mmt_line03"></span>
					</span>
        </a>
        <!-- Menu Mobile End -->
    </div>

    <!-- Mobile View Start-->
    <div class="aurel_mobile_menu_wrapper">
        <!-- Mobile Social Start -->
        <div class="aurel_header_socials">
            <a class="aurel_footer_social_button aurel_facebook" href="https://www.facebook.com/"><i
                        class="fa fa-facebook"></i></a>
            <a class="aurel_footer_social_button aurel_twitter" href="https://twitter.com/"><i
                        class="fa fa-twitter"></i></a>
            <a class="aurel_footer_social_button aurel_google_plus" href="https://plus.google.com/"><i
                        class="fa fa-google-plus"></i></a>
            <a class="aurel_footer_social_button aurel_instagram" href="https://www.instagram.com/?hl=ru"><i
                        class="fa fa-instagram"></i></a>
            <a class="aurel_footer_social_button aurel_pinterest" href="https://pinterest.com/"><i
                        class="fa fa-pinterest"></i></a>
            <a class="aurel_toogle_fullview" href="javascript:void(0)">
                <span class="aurel_fullview_block01"></span>
                <span class="aurel_fullview_block02"></span>
                <span class="aurel_fullview_block03"></span>
                <span class="aurel_fullview_block04"></span>
            </a>
        </div>
        <!-- Mobile Social End -->

        <!-- Mobile Menu Start -->
        <nav class="aurel_mobile_nav">
            <ul class="aurel_menu">
                <li class="menu-item-has-children">
                    <a href="javascript:void(0)">Albums</a>

                    <ul class="sub-menu">
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/albums_grid.html">Grid
                                Albums</a></li>
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/albums_masonry.html">Masonry
                                Albums</a></li>
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/albums_packery.html">Packery
                                Albums</a></li>
                        <li class="menu-item-has-children">
                            <a href="javascript:void(0)">Inside Albums</a>

                            <ul class="sub-menu">
                                <li>
                                    <a href="http://pixel-mafia.com/demo/html-templates/aurel/album_grid.html">Grid</a>
                                </li>
                                <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/album_masonry.html">Masonry</a>
                                </li>
                                <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/album_packery.html">Packery</a>
                                </li>
                                <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/album_kenburns.html">Kenburns</a>
                                </li>
                                <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/album_ribbon.html">Ribbon</a>
                                </li>
                                <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/album_slider.html">Slider</a>
                                </li>
                                <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/album_split.html">Split</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="menu-item-has-children current-menu-ancestor">
                    <a href="javascript:void(0)">Galleries</a>

                    <ul class="sub-menu">
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/gallery_proofing.html">Photo
                                Proofing</a></li>
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/gallery_grid.html">Grid
                                Gallery</a></li>
                        <li class="current-menu-item"><a href="gallery_masonry.html">Masonry Gallery</a></li>
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/gallery_packery.html">Packery
                                Gallery</a></li>
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/gallery_slider.html">Slider
                                Gallery</a></li>
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/gallery_ribbon.html">Ribbon
                                Gallery</a></li>
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/gallery_kenburns.html">Kenburns
                                Gallery</a></li>
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/gallery_split.html">Split
                                Gallery</a></li>
                        <li class="menu-item-has-children">
                            <a href="javascript:void(0)">Video Support</a>

                            <ul class="sub-menu">
                                <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/media_grid.html">Media
                                        Grid</a></li>
                                <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/media_masonry.html">Media
                                        Masonry</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="javascript:void(0)">Best for Portraits</a>

                            <ul class="sub-menu">
                                <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/portrait_slider.html">Portrait
                                        Slider</a></li>
                                <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/portrait_ribbon.html">Portraits
                                        Ribbon</a></li>
                                <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/portrait_grid.html">Portraits
                                        Grid</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="menu-item-has-children">
                    <a href="javascript:void(0)">Features</a>

                    <ul class="sub-menu">
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/shop.html">Digital Store</a>
                        </li>
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/about.html">About Me</a></li>
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/before_after.html">Before /
                                After</a></li>
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/image_background.html">Image
                                Background</a></li>
                        <li>
                            <a href="http://pixel-mafia.com/demo/html-templates/aurel/typography.html">Typography</a>
                        </li>
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/typography_fixed.html">Typography
                                Fixed Page</a></li>
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/book_a_session.html">Book a
                                Session</a></li>
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/clients.html">Happy
                                Clients</a></li>
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/price.html">Price Table</a>
                        </li>
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/coming_soon.html">Coming
                                Soon</a></li>
                    </ul>
                </li>

                <li class="menu-item-has-children">
                    <a href="javascript:void(0)">Blog</a>

                    <ul class="sub-menu">
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/blog_grid.html">Grid Blog</a>
                        </li>
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/blog_full.html">Fullwidth
                                Blog</a></li>
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/blog_right_sidebar.html">Blog
                                with Right Sidebar</a></li>
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/blog_left_sidebar.html">Blog
                                with Left Sidebar</a></li>
                        <li class="menu-item-has-children">
                            <a href="javascript:void(0)">Blog Posts</a>

                            <ul class="sub-menu">
                                <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/blog_standard.html">Standard
                                        Post</a></li>
                                <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/blog_image.html">Image
                                        Post</a></li>
                                <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/blog_gallery.html">Gallery
                                        Post</a></li>
                                <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/blog_video.html">Video
                                        Post</a></li>
                                <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/blog_audio.html">Audio
                                        Post</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="menu-item-has-children">
                    <a href="javascript:void(0)">Contacts</a>

                    <ul class="sub-menu">
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/contacts_1.html">Contacts
                                01</a></li>
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/contacts_2.html">Contacts
                                02</a></li>
                        <li><a href="http://pixel-mafia.com/demo/html-templates/aurel/contacts_3.html">Contacts
                                03</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- Mobile Menu End -->
    </div>
    <!-- Mobile View End -->
</header>