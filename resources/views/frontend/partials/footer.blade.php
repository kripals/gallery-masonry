<!-- Footer Start -->
<footer class="aurel_footer aurel_template_footer_solid aurel_border_on">
    <div class="aurel_footer_inner aurel_container">
        <div class="aurel_logo_cont">
            <a href="" class="aurel_image_logo aurel_dp aurel_no_select aurel_retina"></a>
        </div>

        <div class="aurel_copyright">© 2018 Photo Aurel. All Rights Reserved.</div>
    </div>
</footer>
<!-- Footer End -->