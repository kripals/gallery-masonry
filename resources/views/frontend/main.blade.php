<!DOCTYPE html>
<html lang="en" class="aurel_header_gradient_page aurel_content_under_header aurel_non_transparent_header">

<!-- Mirrored from pixel-mafia.com/demo/html-templates/aurel/gallery_masonry.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 02 Aug 2018 08:48:57 GMT -->
<head>
    <title>Masonry Gallery</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700,900" rel="stylesheet">

    <!-- CSS Files Start -->
    <link rel="stylesheet" href="{{ asset('css/kube.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/photoswipe.css') }}">
    <link rel="stylesheet" href="{{ asset('css/default-skin.css') }}">
    <!-- CSS Files End-->

    <!-- Google Analytics Start -->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'js/analytics.js', 'ga');

        ga('create', 'UA-70926907-6', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- Google Analytics End -->

</head>

<body class="aurel_drag_protection">
<div class="aurel_site_wrapper static_header_footer fadeOnLoad">

@include('frontend.partials.header')

<!-- Content Start -->
    <div class="aurel_main_wrapper aurel_top_padding_no aurel_bottom_padding_no">
        <div class="aurel_container">
            <div class="aurel_content_wrapper row aurel_no_sidebar">
                <div class="aurel_content col col-12">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    <!-- Content End -->

    @include('frontend.partials.footer')

<!-- Back Button -->
    <div class="aurel_back_to_top"></div>
</div>

<!-- Pre loader Start -->
<div class="aurel_preloader_wrapper">
    <div class="aurel_preloader_content">
        <div class="aurel_preloader_bar"></div>
        <h6 class="aurel_preloader_text">LOADING</h6>
    </div>
</div>
<!-- Pre loader End -->

<!-- JS Files Start -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery.swipebox.js') }}"></script>
<script src="{{ asset('js/jquery.fullscreen.min.js') }}"></script>
<script src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ asset('js/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('js/photoswipe.min.js') }}"></script>
<script src="{{ asset('js/photoswipe-ui-default.min.js') }}"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/theme.js') }}"></script>
<!-- JS Files End -->

</body>

</html>