@extends('frontend.main')
@section('content')
    <div class="row aurel_js_bg_image aurel_js_min_height aurel_bg_center_center aurel_bg_size_cover aurel_pf_fullwidth aurel_mb_40"
         data-src="img/clipart/back_7.jpg" data-min-height="650">
        <div class="col col-12 push-middle">
            <h1 class="aurel_page_heading">Masonry Gallery</h1>
        </div>
    </div>
    <div class="row aurel_pf_fullwidth">
        <div class="col col-12 aurel_pl_30 aurel_pr_30">
            <div class="aurel_widget_pm_grid_gallery">
                <div class="aurel_widget_container">
                    <div class="aurel_front_end_display">
                        <div class="aurel_grid_wrapper aurel_photoswipe_wrapper aurel_grid_007"
                             data-uniqid="007" data-perload="4">
                            <div class="aurel_grid_inner aurel_isotope_trigger is_masonry grid_columns4 hover_type_solid_plus side_paddings_off"
                                 data-perload="4" data-pad="30">
                                <!-- Item 1 -->
                                <div class="grid-item element grid_b2p">
                                    <div class="grid-item-inner">
                                        <a class="aurel_pswp_slide aurel_drag_protect aurel_dp aurel_no_select"
                                           href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/masonry_gallery/img-1.jpg"
                                           data-size="1200x800" data-count="0">
                                            <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/masonry_gallery/thumb/thumb-1.jpg"
                                                 class="grid_thmb" alt=""/>

                                            <div class="grid-item-content">
                                                <h4>Road to Home</h4>
                                            </div>

                                            <div class="grid-item-overlay aurel_js_bg_color"
                                                 data-bgcolor="rgba(17,17,17,0.3)"></div>
                                        </a>

                                        <div class="aurel-img-preloader"></div>
                                    </div>
                                </div>

                                <!-- Item 2 -->
                                <div class="grid-item element grid_b2p">
                                    <div class="grid-item-inner">
                                        <a class="aurel_pswp_slide aurel_drag_protect aurel_dp aurel_no_select"
                                           href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/masonry_gallery/img-2.jpg"
                                           data-size="1200x1400" data-count="1">
                                            <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/masonry_gallery/thumb/thumb-2.jpg"
                                                 class="grid_thmb" alt=""/>

                                            <div class="grid-item-content">
                                                <h4>Waterfall</h4>
                                            </div>

                                            <div class="grid-item-overlay aurel_js_bg_color"
                                                 data-bgcolor="rgba(17,17,17,0.3)"></div>
                                        </a>

                                        <div class="aurel-img-preloader"></div>
                                    </div>
                                </div>

                                <!-- Item 3 -->
                                <div class="grid-item element grid_b2p">
                                    <div class="grid-item-inner">
                                        <a class="aurel_pswp_slide aurel_drag_protect aurel_dp aurel_no_select"
                                           href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/masonry_gallery/img-3.jpg"
                                           data-size="1200x800" data-count="2">
                                            <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/masonry_gallery/thumb/thumb-3.jpg"
                                                 class="grid_thmb" alt=""/>

                                            <div class="grid-item-content">
                                                <h4>Road to Home</h4>
                                            </div>

                                            <div class="grid-item-overlay aurel_js_bg_color"
                                                 data-bgcolor="rgba(17,17,17,0.3)"></div>
                                        </a>

                                        <div class="aurel-img-preloader"></div>
                                    </div>
                                </div>

                                <!-- Item 4 -->
                                <div class="grid-item element grid_b2p">
                                    <div class="grid-item-inner">
                                        <a class="aurel_pswp_slide aurel_drag_protect aurel_dp aurel_no_select"
                                           href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/masonry_gallery/img-4.jpg"
                                           data-size="1200x1200" data-count="3">
                                            <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/masonry_gallery/thumb/thumb-4.jpg"
                                                 class="grid_thmb" alt=""/>

                                            <div class="grid-item-content">
                                                <h4>Village Road</h4>
                                            </div>

                                            <div class="grid-item-overlay aurel_js_bg_color"
                                                 data-bgcolor="rgba(17,17,17,0.3)"></div>
                                        </a>

                                        <div class="aurel-img-preloader"></div>
                                    </div>
                                </div>

                                <!-- Item 5 -->
                                <div class="grid-item element grid_b2p">
                                    <div class="grid-item-inner">
                                        <a class="aurel_pswp_slide aurel_drag_protect aurel_dp aurel_no_select"
                                           href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/masonry_gallery/img-5.jpg"
                                           data-size="1200x1400" data-count="4">
                                            <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/masonry_gallery/thumb/thumb-5.jpg"
                                                 class="grid_thmb" alt=""/>

                                            <div class="grid-item-content">
                                                <h4>Road to Home</h4>
                                            </div>

                                            <div class="grid-item-overlay aurel_js_bg_color"
                                                 data-bgcolor="rgba(17,17,17,0.3)"></div>
                                        </a>

                                        <div class="aurel-img-preloader"></div>
                                    </div>
                                </div>

                                <!-- Item 6 -->
                                <div class="grid-item element grid_b2p">
                                    <div class="grid-item-inner">
                                        <a class="aurel_pswp_slide aurel_drag_protect aurel_dp aurel_no_select"
                                           href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/masonry_gallery/img-6.jpg"
                                           data-size="1200x1400" data-count="5">
                                            <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/masonry_gallery/thumb/thumb-6.jpg"
                                                 class="grid_thmb" alt=""/>

                                            <div class="grid-item-content">
                                                <h4>Road to Home</h4>
                                            </div>

                                            <div class="grid-item-overlay aurel_js_bg_color"
                                                 data-bgcolor="rgba(17,17,17,0.3)"></div>
                                        </a>

                                        <div class="aurel-img-preloader"></div>
                                    </div>
                                </div>

                                <!-- Item 7 -->
                                <div class="grid-item element grid_b2p">
                                    <div class="grid-item-inner">
                                        <a class="aurel_pswp_slide aurel_drag_protect aurel_dp aurel_no_select"
                                           href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/masonry_gallery/img-7.jpg"
                                           data-size="1200x800" data-count="6">
                                            <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/masonry_gallery/thumb/thumb-7.jpg"
                                                 class="grid_thmb" alt=""/>

                                            <div class="grid-item-content">
                                                <h4>Majestic Mountains</h4>
                                            </div>

                                            <div class="grid-item-overlay aurel_js_bg_color"
                                                 data-bgcolor="rgba(17,17,17,0.3)"></div>
                                        </a>

                                        <div class="aurel-img-preloader"></div>
                                    </div>
                                </div>

                                <!-- Item 8 -->
                                <div class="grid-item element grid_b2p">
                                    <div class="grid-item-inner">
                                        <a class="aurel_pswp_slide aurel_drag_protect aurel_dp aurel_no_select"
                                           href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/masonry_gallery/img-8.jpg"
                                           data-size="1200x1200" data-count="7">
                                            <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/masonry_gallery/thumb/thumb-8.jpg"
                                                 class="grid_thmb" alt=""/>

                                            <div class="grid-item-content">
                                                <h4>Road to Home</h4>
                                            </div>

                                            <div class="grid-item-overlay aurel_js_bg_color"
                                                 data-bgcolor="rgba(17,17,17,0.3)"></div>
                                        </a>

                                        <div class="aurel-img-preloader"></div>
                                    </div>
                                </div>

                                <!-- Item 9 -->
                                <div class="grid-item element grid_b2p">
                                    <div class="grid-item-inner">
                                        <a class="aurel_pswp_slide aurel_drag_protect aurel_dp aurel_no_select"
                                           href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/masonry_gallery/img-9.jpg"
                                           data-size="1200x1400" data-count="8">
                                            <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/masonry_gallery/thumb/thumb-9.jpg"
                                                 class="grid_thmb" alt=""/>

                                            <div class="grid-item-content">
                                                <h4>Road to Home</h4>
                                            </div>

                                            <div class="grid-item-overlay aurel_js_bg_color"
                                                 data-bgcolor="rgba(17,17,17,0.3)"></div>
                                        </a>

                                        <div class="aurel-img-preloader"></div>
                                    </div>
                                </div>

                                <!-- Item 10 -->
                                <div class="grid-item element grid_b2p">
                                    <div class="grid-item-inner">
                                        <a class="aurel_pswp_slide aurel_drag_protect aurel_dp aurel_no_select"
                                           href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/masonry_gallery/img-10.jpg"
                                           data-size="1200x1200" data-count="9">
                                            <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/masonry_gallery/thumb/thumb-10.jpg"
                                                 class="grid_thmb" alt=""/>

                                            <div class="grid-item-content">
                                                <h4>In the Forest</h4>
                                            </div>

                                            <div class="grid-item-overlay aurel_js_bg_color"
                                                 data-bgcolor="rgba(17,17,17,0.3)"></div>
                                        </a>

                                        <div class="aurel-img-preloader"></div>
                                    </div>
                                </div>

                                <!-- Item 11 -->
                                <div class="grid-item element grid_b2p">
                                    <div class="grid-item-inner">
                                        <a class="aurel_pswp_slide aurel_drag_protect aurel_dp aurel_no_select"
                                           href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/masonry_gallery/img-11.jpg"
                                           data-size="1200x1200" data-count="10">
                                            <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/masonry_gallery/thumb/thumb-11.jpg"
                                                 class="grid_thmb" alt=""/>

                                            <div class="grid-item-content">
                                                <h4>Road to Home</h4>
                                            </div>

                                            <div class="grid-item-overlay aurel_js_bg_color"
                                                 data-bgcolor="rgba(17,17,17,0.3)"></div>
                                        </a>

                                        <div class="aurel-img-preloader"></div>
                                    </div>
                                </div>

                                <!-- Item 12 -->
                                <div class="grid-item element grid_b2p">
                                    <div class="grid-item-inner">
                                        <a class="aurel_pswp_slide aurel_drag_protect aurel_dp aurel_no_select"
                                           href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/masonry_gallery/img-12.jpg"
                                           data-size="1200x800" data-count="11">
                                            <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/masonry_gallery/thumb/thumb-12.jpg"
                                                 class="grid_thmb" alt=""/>

                                            <div class="grid-item-content">
                                                <h4>Road to Home</h4>
                                            </div>

                                            <div class="grid-item-overlay aurel_js_bg_color"
                                                 data-bgcolor="rgba(17,17,17,0.3)"></div>
                                        </a>

                                        <div class="aurel-img-preloader"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection