@extends('frontend.main')
@section('content')
    <div class="aurel_tiny">
        <div class="row aurel_js_bg_image aurel_js_min_height aurel_bg_center_center aurel_bg_size_cover aurel_pf_fullwidth aurel_mb_40" data-src="img/clipart/back_10.jpg" data-min-height="650">
            <div class="col col-12 push-middle aurel_sm_pl_20 aurel_sm_pr_20">
                <h1 class="aurel_page_heading ">Photo Proofing</h1>
                <div class="aurel_post_meta aurel_text_align_center aurel_mb_20">
                    <div class="aurel_post_meta_item aurel_js_font_size aurel_sm_font_16" data-font-size="18px" data-line-height="28px"><i>Client: Eddie Howard</i></div>
                    <div class="aurel_post_meta_item aurel_js_font_size aurel_sm_font_16" data-font-size="18px" data-line-height="28px"><i>Date: 11/06/2018</i></div>
                </div>

                <div class="row">
                    <div class="col col-6 push-center aurel_md_width_80">
                        <p class="aurel_text_align_center aurel_js_font_size aurel_js_color aurel_sm_font_15" data-font-size="18px" data-line-height="28px" data-color="#fff">
                            Send a secure link with the photos to your clients, so they can select the needed ones for printing or buying. After that you are going to receive email notification with the link to the page. There you find all selected by your customer’s photos and the ability to sort them according to your needs.
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row aurel_pf_fullwidth">
            <div class="col col-12 aurel_pl_30 aurel_pr_30">
                <div class="aurel_widget_pm_proofing_gallery">
                    <div class="aurel_widget_container">
                        <div class="aurel_front_end_display">
                            <ul id="filters" class="aurel_grid_filter">
                                <li class="aurel_filter-item"><a href="javascript:void(0)" data-category="*">All</a></li>
                                <li class="aurel_filter-item"><a href="javascript:void(0)" data-category=".approved">Approved</a></li>
                                <li class="aurel_filter-item"><a href="javascript:void(0)" data-category=".unapproved">Unapproved</a></li>
                                <li class="aurel_filter-item"><a href="javascript:void(0)" data-category=".unviewed">Unviewed</a></li>
                            </ul>

                            <div class="aurel_grid_wrapper aurel_photoswipe_wrapper aurel_grid_3368 aurel_no_select aurel_dp" data-uniqid="3368">
                                <div class="aurel_grid_inner aurel_proofing_grid aurel_isotope_trigger grid_columns4 side_paddings_off" data-pad="30">
                                    <!-- Item 1 -->
                                    <div class="grid-item grid-item-proofing element grid_b2p approved">
                                        <div class="grid-item-inner">
                                            <div class="aurel_proofing_grid_item">
                                                <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/thumb/thumb-1.jpg" class="grid_thmb" alt=""/>

                                                <div class="grid-item-content">
                                                    <h4>Sweet Blonde</h4>
                                                </div>

                                                <div class="grid-item-overlay aurel_js_bg_color" data-bgcolor="rgba(17,17,17,0.5)"></div>

                                                <div class="grid-item-controls">
                                                    <a class="aurel_pswp_slide grid-item-button-zoom" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-1.jpg" data-size="1200x1400" data-count="0"><i class="fa fa-search"></i></a>
                                                    <a class="grid-item-button-download" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-1.jpg" download><i class="fa fa-download"></i></a>
                                                </div>
                                            </div>

                                            <div class="aurel-img-preloader"></div>
                                        </div>
                                    </div>

                                    <!-- Item 2 -->
                                    <div class="grid-item grid-item-proofing element grid_b2p approved">
                                        <div class="grid-item-inner">
                                            <div class="aurel_proofing_grid_item">
                                                <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/thumb/thumb-2.jpg" class="grid_thmb" alt=""/>

                                                <div class="grid-item-content">
                                                    <h4>Road to Home</h4>
                                                </div>

                                                <div class="grid-item-overlay aurel_js_bg_color" data-bgcolor="rgba(17,17,17,0.5)"></div>

                                                <div class="grid-item-controls">
                                                    <a class="aurel_pswp_slide grid-item-button-zoom" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-2.jpg" data-size="1200x800" data-count="1"><i class="fa fa-search"></i></a>
                                                    <a class="grid-item-button-download" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-2.jpg" download><i class="fa fa-download"></i></a>
                                                </div>
                                            </div>

                                            <div class="aurel-img-preloader"></div>
                                        </div>
                                    </div>

                                    <!-- Item 3 -->
                                    <div class="grid-item grid-item-proofing element grid_b2p approved">
                                        <div class="grid-item-inner">
                                            <div class="aurel_proofing_grid_item">
                                                <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/thumb/thumb-3.jpg" class="grid_thmb" alt=""/>

                                                <div class="grid-item-content">
                                                    <h4>Sport Fashion</h4>
                                                </div>

                                                <div class="grid-item-overlay aurel_js_bg_color" data-bgcolor="rgba(17,17,17,0.5)"></div>

                                                <div class="grid-item-controls">
                                                    <a class="aurel_pswp_slide grid-item-button-zoom" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-3.jpg" data-size="1200x800" data-count="2"><i class="fa fa-search"></i></a>
                                                    <a class="grid-item-button-download" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-3.jpg" download><i class="fa fa-download"></i></a>
                                                </div>

                                            </div>

                                            <div class="aurel-img-preloader"></div>
                                        </div>
                                    </div>

                                    <!-- Item 4 -->
                                    <div class="grid-item grid-item-proofing element grid_b2p unapproved">
                                        <div class="grid-item-inner">
                                            <div class="aurel_proofing_grid_item">
                                                <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/thumb/thumb-4.jpg" class="grid_thmb" alt=""/>

                                                <div class="grid-item-content">
                                                    <h4>Small Things</h4>
                                                </div>

                                                <div class="grid-item-overlay aurel_js_bg_color" data-bgcolor="rgba(17,17,17,0.5)"></div>

                                                <div class="grid-item-controls">
                                                    <a class="aurel_pswp_slide grid-item-button-zoom" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-4.jpg" data-size="1200x1200" data-count="3"><i class="fa fa-search"></i></a>
                                                    <a class="grid-item-button-download" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-4.jpg" download><i class="fa fa-download"></i></a>
                                                </div>
                                            </div>

                                            <div class="aurel-img-preloader"></div>
                                        </div>
                                    </div>

                                    <!-- Item 5 -->
                                    <div class="grid-item grid-item-proofing element grid_b2p approved">
                                        <div class="grid-item-inner">
                                            <div class="aurel_proofing_grid_item">
                                                <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/thumb/thumb-5.jpg" class="grid_thmb" alt=""/>

                                                <div class="grid-item-content">
                                                    <h4>Little Pumpkin</h4>
                                                </div>

                                                <div class="grid-item-overlay aurel_js_bg_color" data-bgcolor="rgba(17,17,17,0.5)"></div>

                                                <div class="grid-item-controls">
                                                    <a class="aurel_pswp_slide grid-item-button-zoom" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-5.jpg" data-size="1200x1400" data-count="4"><i class="fa fa-search"></i></a>
                                                    <a class="grid-item-button-download" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-5.jpg" download><i class="fa fa-download"></i></a>
                                                    <a class="grid-item-button-unapprove" href="javascript:void(0)" data-moduleid="3368" data-uniqid="05"><i class="fa fa-times-circle"></i></a>
                                                    <a class="grid-item-button-approve" href="javascript:void(0)" data-moduleid="3368" data-uniqid="05"><i class="fa fa-minus-circle"></i></a>
                                                </div>

                                                <div class="grid-icon-state grid-icon-approve"><i class=""></i></div>

                                                <div class="grid-icon-state grid-icon-unaprove"><i class="fa fa-times"></i></div>
                                            </div>

                                            <div class="aurel-img-preloader"></div>
                                        </div>
                                    </div>

                                    <!-- Item 6 -->
                                    <div class="grid-item grid-item-proofing element grid_b2p approved">
                                        <div class="grid-item-inner">
                                            <div class="aurel_proofing_grid_item">
                                                <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/thumb/thumb-6.jpg" class="grid_thmb" alt=""/>

                                                <div class="grid-item-content">
                                                    <h4>Smoking Girl</h4>
                                                </div>

                                                <div class="grid-item-overlay aurel_js_bg_color" data-bgcolor="rgba(17,17,17,0.5)"></div>

                                                <div class="grid-item-controls">
                                                    <a class="aurel_pswp_slide grid-item-button-zoom" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-6.jpg" data-size="1200x1400" data-count="5"><i class="fa fa-search"></i></a>
                                                    <a class="grid-item-button-download" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-6.jpg" download><i class="fa fa-download"></i></a>
                                                    <a class="grid-item-button-unapprove" href="javascript:void(0)" data-moduleid="3368" data-uniqid="06"><i class="fa fa-times-circle"></i></a>
                                                    <a class="grid-item-button-approve" href="javascript:void(0)" data-moduleid="3368" data-uniqid="06"><i class="fa fa-minus-circle"></i></a>
                                                </div>

                                                <div class="grid-icon-state grid-icon-approve"><i class=""></i></div>

                                                <div class="grid-icon-state grid-icon-unaprove"><i class="fa fa-times"></i></div>
                                            </div>

                                            <div class="aurel-img-preloader"></div>
                                        </div>
                                    </div>

                                    <!-- Item 7 -->
                                    <div class="grid-item grid-item-proofing element grid_b2p approved">
                                        <div class="grid-item-inner">
                                            <div class="aurel_proofing_grid_item">
                                                <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/thumb/thumb-7.jpg" class="grid_thmb" alt=""/>

                                                <div class="grid-item-content">
                                                    <h4>In the Shadows</h4>
                                                </div>

                                                <div class="grid-item-overlay aurel_js_bg_color" data-bgcolor="rgba(17,17,17,0.5)"></div>

                                                <div class="grid-item-controls">
                                                    <a class="aurel_pswp_slide grid-item-button-zoom" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-7.jpg" data-size="1200x1400" data-count="6"><i class="fa fa-search"></i></a>
                                                    <a class="grid-item-button-download" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-7.jpg" download><i class="fa fa-download"></i></a>
                                                    <a class="grid-item-button-unapprove" href="javascript:void(0)" data-moduleid="3368" data-uniqid="07"><i class="fa fa-times-circle"></i></a>
                                                    <a class="grid-item-button-approve" href="javascript:void(0)" data-moduleid="3368" data-uniqid="07"><i class="fa fa-minus-circle"></i></a>
                                                </div>

                                                <div class="grid-icon-state grid-icon-approve"><i class=""></i></div>

                                                <div class="grid-icon-state grid-icon-unaprove"><i class="fa fa-times"></i></div>
                                            </div>

                                            <div class="aurel-img-preloader"></div>
                                        </div>
                                    </div>

                                    <!-- Item 8 -->
                                    <div class="grid-item grid-item-proofing element grid_b2p approved">
                                        <div class="grid-item-inner">
                                            <div class="aurel_proofing_grid_item">
                                                <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/thumb/thumb-8.jpg" class="grid_thmb" alt=""/>

                                                <div class="grid-item-content">
                                                    <h4>Majestic Mountains</h4>
                                                </div>

                                                <div class="grid-item-overlay aurel_js_bg_color" data-bgcolor="rgba(17,17,17,0.5)"></div>

                                                <div class="grid-item-controls">
                                                    <a class="aurel_pswp_slide grid-item-button-zoom" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-8.jpg" data-size="1200x800" data-count="7"><i class="fa fa-search"></i></a>
                                                    <a class="grid-item-button-download" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-8.jpg" download><i class="fa fa-download"></i></a>
                                                    <a class="grid-item-button-unapprove" href="javascript:void(0)" data-moduleid="3368" data-uniqid="08"><i class="fa fa-times-circle"></i></a>
                                                    <a class="grid-item-button-approve" href="javascript:void(0)" data-moduleid="3368" data-uniqid="08"><i class="fa fa-minus-circle"></i></a>
                                                </div>

                                                <div class="grid-icon-state grid-icon-approve"><i class=""></i></div>

                                                <div class="grid-icon-state grid-icon-unaprove"><i class="fa fa-times"></i></div>
                                            </div>

                                            <div class="aurel-img-preloader"></div>
                                        </div>
                                    </div>

                                    <!-- Item 9 -->
                                    <div class="grid-item grid-item-proofing element grid_b2p approved">
                                        <div class="grid-item-inner">
                                            <div class="aurel_proofing_grid_item">
                                                <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/thumb/thumb-9.jpg" class="grid_thmb" alt=""/>

                                                <div class="grid-item-content">
                                                    <h4>In the Forest</h4>
                                                </div>

                                                <div class="grid-item-overlay aurel_js_bg_color" data-bgcolor="rgba(17,17,17,0.5)"></div>

                                                <div class="grid-item-controls">
                                                    <a class="aurel_pswp_slide grid-item-button-zoom" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-9.jpg" data-size="1200x1200" data-count="8"><i class="fa fa-search"></i></a>
                                                    <a class="grid-item-button-download" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-9.jpg" download><i class="fa fa-download"></i></a>
                                                    <a class="grid-item-button-unapprove" href="javascript:void(0)" data-moduleid="3368" data-uniqid="09"><i class="fa fa-times-circle"></i></a>
                                                    <a class="grid-item-button-approve" href="javascript:void(0)" data-moduleid="3368" data-uniqid="09"><i class="fa fa-minus-circle"></i></a>
                                                </div>

                                                <div class="grid-icon-state grid-icon-approve"><i class=""></i></div>

                                                <div class="grid-icon-state grid-icon-unaprove"><i class="fa fa-times"></i></div>
                                            </div>

                                            <div class="aurel-img-preloader"></div>
                                        </div>
                                    </div>

                                    <!-- Item 10 -->
                                    <div class="grid-item grid-item-proofing element grid_b2p unapproved">
                                        <div class="grid-item-inner">
                                            <div class="aurel_proofing_grid_item">
                                                <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/thumb/thumb-10.jpg" class="grid_thmb" alt=""/>

                                                <div class="grid-item-content">
                                                    <h4>Light My Fire</h4>
                                                </div>

                                                <div class="grid-item-overlay aurel_js_bg_color" data-bgcolor="rgba(17,17,17,0.5)"></div>

                                                <div class="grid-item-controls">
                                                    <a class="aurel_pswp_slide grid-item-button-zoom" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-10.jpg" data-size="1200x800" data-count="9"><i class="fa fa-search"></i></a>
                                                    <a class="grid-item-button-download" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-10.jpg" download><i class="fa fa-download"></i></a>
                                                    <a class="grid-item-button-unapprove" href="javascript:void(0)" data-moduleid="3368" data-uniqid="010"><i class="fa fa-minus-circle"></i></a>
                                                    <a class="grid-item-button-approve" href="javascript:void(0)" data-moduleid="3368" data-uniqid="010"><i class="fa fa-check-circle"></i></a>
                                                </div>

                                                <div class="grid-icon-state grid-icon-approve"><i class=""></i></div>

                                                <div class="grid-icon-state grid-icon-unaprove"><i class="fa fa-times"></i></div>
                                            </div>

                                            <div class="aurel-img-preloader"></div>
                                        </div>
                                    </div>

                                    <!-- Item 11 -->
                                    <div class="grid-item grid-item-proofing element grid_b2p unapproved">
                                        <div class="grid-item-inner">
                                            <div class="aurel_proofing_grid_item">
                                                <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/thumb/thumb-11.jpg" class="grid_thmb" alt=""/>

                                                <div class="grid-item-content">
                                                    <h4>Still Life</h4>
                                                </div>

                                                <div class="grid-item-overlay aurel_js_bg_color" data-bgcolor="rgba(17,17,17,0.5)"></div>

                                                <div class="grid-item-controls">
                                                    <a class="aurel_pswp_slide grid-item-button-zoom" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-11.jpg" data-size="1200x1200" data-count="10"><i class="fa fa-search"></i></a>
                                                    <a class="grid-item-button-download" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-11.jpg" download><i class="fa fa-download"></i></a>
                                                </div>


                                            </div>

                                            <div class="aurel-img-preloader"></div>
                                        </div>
                                    </div>

                                    <!-- Item 12 -->
                                    <div class="grid-item grid-item-proofing element grid_b2p approved">
                                        <div class="grid-item-inner">
                                            <div class="aurel_proofing_grid_item">
                                                <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/thumb/thumb-12.jpg" class="grid_thmb" alt=""/>

                                                <div class="grid-item-content">
                                                    <h4>Beautiful Ringlets</h4>
                                                </div>

                                                <div class="grid-item-overlay aurel_js_bg_color" data-bgcolor="rgba(17,17,17,0.5)"></div>

                                                <div class="grid-item-controls">
                                                    <a class="aurel_pswp_slide grid-item-button-zoom" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-12.jpg" data-size="1200x1400" data-count="11"><i class="fa fa-search"></i></a>
                                                    <a class="grid-item-button-download" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-12.jpg" download><i class="fa fa-download"></i></a>
                                                    <a class="grid-item-button-unapprove" href="javascript:void(0)" data-moduleid="3368" data-uniqid="012"><i class="fa fa-times-circle"></i></a>
                                                    <a class="grid-item-button-approve" href="javascript:void(0)" data-moduleid="3368" data-uniqid="012"><i class="fa fa-minus-circle"></i></a>
                                                </div>

                                                <div class="grid-icon-state grid-icon-approve"><i class=""></i></div>

                                                <div class="grid-icon-state grid-icon-unaprove"><i class="fa fa-times"></i></div>
                                            </div>

                                            <div class="aurel-img-preloader"></div>
                                        </div>
                                    </div>

                                    <!-- Item 13 -->
                                    <div class="grid-item grid-item-proofing element grid_b2p unapproved">
                                        <div class="grid-item-inner">
                                            <div class="aurel_proofing_grid_item">
                                                <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/thumb/thumb-13.jpg" class="grid_thmb" alt=""/>

                                                <div class="grid-item-content">
                                                    <h4>Autumn Evening</h4>
                                                </div>

                                                <div class="grid-item-overlay aurel_js_bg_color" data-bgcolor="rgba(17,17,17,0.5)"></div>

                                                <div class="grid-item-controls">
                                                    <a class="aurel_pswp_slide grid-item-button-zoom" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-13.jpg" data-size="1200x1000" data-count="12"><i class="fa fa-search"></i></a>
                                                    <a class="grid-item-button-download" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-13.jpg" download><i class="fa fa-download"></i></a>
                                                    <a class="grid-item-button-unapprove" href="javascript:void(0)" data-moduleid="3368" data-uniqid="013"><i class="fa fa-minus-circle"></i></a>
                                                    <a class="grid-item-button-approve" href="javascript:void(0)" data-moduleid="3368" data-uniqid="013"><i class="fa fa-check-circle"></i></a>
                                                </div>

                                                <div class="grid-icon-state grid-icon-approve"><i class=""></i></div>

                                                <div class="grid-icon-state grid-icon-unaprove"><i class="fa fa-times"></i></div>
                                            </div>

                                            <div class="aurel-img-preloader"></div>
                                        </div>
                                    </div>

                                    <!-- Item 14 -->
                                    <div class="grid-item grid-item-proofing element grid_b2p approved">
                                        <div class="grid-item-inner">
                                            <div class="aurel_proofing_grid_item">
                                                <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/thumb/thumb-14.jpg" class="grid_thmb" alt=""/>

                                                <div class="grid-item-content">
                                                    <h4>Business Style</h4>
                                                </div>

                                                <div class="grid-item-overlay aurel_js_bg_color" data-bgcolor="rgba(17,17,17,0.5)"></div>

                                                <div class="grid-item-controls">
                                                    <a class="aurel_pswp_slide grid-item-button-zoom" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-14.jpg" data-size="1200x1200" data-count="13"><i class="fa fa-search"></i></a>
                                                    <a class="grid-item-button-download" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-14.jpg" download><i class="fa fa-download"></i></a>
                                                    <a class="grid-item-button-unapprove" href="javascript:void(0)" data-moduleid="3368" data-uniqid="014"><i class="fa fa-times-circle"></i></a>
                                                    <a class="grid-item-button-approve" href="javascript:void(0)" data-moduleid="3368" data-uniqid="014"><i class="fa fa-minus-circle"></i></a>
                                                </div>

                                                <div class="grid-icon-state grid-icon-approve"><i class=""></i></div>

                                                <div class="grid-icon-state grid-icon-unaprove"><i class="fa fa-times"></i></div>
                                            </div>

                                            <div class="aurel-img-preloader"></div>
                                        </div>
                                    </div>

                                    <!-- Item 15 -->
                                    <div class="grid-item grid-item-proofing element grid_b2p approved">
                                        <div class="grid-item-inner">
                                            <div class="aurel_proofing_grid_item">
                                                <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/thumb/thumb-15.jpg" class="grid_thmb" alt=""/>

                                                <div class="grid-item-content">
                                                    <h4>Autumn Stuff</h4>
                                                </div>

                                                <div class="grid-item-overlay aurel_js_bg_color" data-bgcolor="rgba(17,17,17,0.5)"></div>

                                                <div class="grid-item-controls">
                                                    <a class="aurel_pswp_slide grid-item-button-zoom" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-15.jpg" data-size="1200x800" data-count="14"><i class="fa fa-search"></i></a>
                                                    <a class="grid-item-button-download" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-15.jpg" download><i class="fa fa-download"></i></a>
                                                    <a class="grid-item-button-unapprove" href="javascript:void(0)" data-moduleid="3368" data-uniqid="015"><i class="fa fa-times-circle"></i></a>
                                                    <a class="grid-item-button-approve" href="javascript:void(0)" data-moduleid="3368" data-uniqid="015"><i class="fa fa-minus-circle"></i></a>
                                                </div>

                                                <div class="grid-icon-state grid-icon-approve"><i class=""></i></div>

                                                <div class="grid-icon-state grid-icon-unaprove"><i class="fa fa-times"></i></div>
                                            </div>

                                            <div class="aurel-img-preloader"></div>
                                        </div>
                                    </div>

                                    <!-- Item 16 -->
                                    <div class="grid-item grid-item-proofing element grid_b2p approved">
                                        <div class="grid-item-inner">
                                            <div class="aurel_proofing_grid_item">
                                                <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/thumb/thumb-16.jpg" class="grid_thmb" alt=""/>

                                                <div class="grid-item-content">
                                                    <h4>Rope Bundle</h4>
                                                </div>

                                                <div class="grid-item-overlay aurel_js_bg_color" data-bgcolor="rgba(17,17,17,0.5)"></div>

                                                <div class="grid-item-controls">
                                                    <a class="aurel_pswp_slide grid-item-button-zoom" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-16.jpg" data-size="1200x1000" data-count="15"><i class="fa fa-search"></i></a>
                                                    <a class="grid-item-button-download" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-16.jpg" download><i class="fa fa-download"></i></a>
                                                    <a class="grid-item-button-unapprove" href="javascript:void(0)" data-moduleid="3368" data-uniqid="016"><i class="fa fa-times-circle"></i></a>
                                                    <a class="grid-item-button-approve" href="javascript:void(0)" data-moduleid="3368" data-uniqid="016"><i class="fa fa-minus-circle"></i></a>
                                                </div>

                                                <div class="grid-icon-state grid-icon-approve"><i class=""></i></div>

                                                <div class="grid-icon-state grid-icon-unaprove"><i class="fa fa-times"></i></div>
                                            </div>

                                            <div class="aurel-img-preloader"></div>
                                        </div>
                                    </div>

                                    <!-- Item 17 -->
                                    <div class="grid-item grid-item-proofing element grid_b2p approved">
                                        <div class="grid-item-inner">
                                            <div class="aurel_proofing_grid_item">
                                                <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/thumb/thumb-17.jpg" class="grid_thmb" alt=""/>

                                                <div class="grid-item-content">
                                                    <h4>Breakfast</h4>
                                                </div>

                                                <div class="grid-item-overlay aurel_js_bg_color" data-bgcolor="rgba(17,17,17,0.5)"></div>

                                                <div class="grid-item-controls">
                                                    <a class="aurel_pswp_slide grid-item-button-zoom" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-17.jpg" data-size="1200x1000" data-count="16"><i class="fa fa-search"></i></a>
                                                    <a class="grid-item-button-download" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-17.jpg" download><i class="fa fa-download"></i></a>
                                                    <a class="grid-item-button-unapprove" href="javascript:void(0)" data-moduleid="3368" data-uniqid="017"><i class="fa fa-times-circle"></i></a>
                                                    <a class="grid-item-button-approve" href="javascript:void(0)" data-moduleid="3368" data-uniqid="017"><i class="fa fa-minus-circle"></i></a>
                                                </div>

                                                <div class="grid-icon-state grid-icon-approve"><i class=""></i></div>

                                                <div class="grid-icon-state grid-icon-unaprove"><i class="fa fa-times"></i></div>
                                            </div>

                                            <div class="aurel-img-preloader"></div>
                                        </div>
                                    </div>

                                    <!-- Item 18 -->
                                    <div class="grid-item grid-item-proofing element grid_b2p approved">
                                        <div class="grid-item-inner">
                                            <div class="aurel_proofing_grid_item">
                                                <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/thumb/thumb-18.jpg" class="grid_thmb" alt=""/>

                                                <div class="grid-item-content">
                                                    <h4>Fashion Jeans</h4>
                                                </div>

                                                <div class="grid-item-overlay aurel_js_bg_color" data-bgcolor="rgba(17,17,17,0.5)"></div>

                                                <div class="grid-item-controls">
                                                    <a class="aurel_pswp_slide grid-item-button-zoom" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-18.jpg" data-size="1200x1200" data-count="17"><i class="fa fa-search"></i></a>
                                                    <a class="grid-item-button-download" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-18.jpg" download><i class="fa fa-download"></i></a>
                                                    <a class="grid-item-button-unapprove" href="javascript:void(0)" data-moduleid="3368" data-uniqid="018"><i class="fa fa-times-circle"></i></a>
                                                    <a class="grid-item-button-approve" href="javascript:void(0)" data-moduleid="3368" data-uniqid="018"><i class="fa fa-minus-circle"></i></a>
                                                </div>

                                                <div class="grid-icon-state grid-icon-approve"><i class=""></i></div>

                                                <div class="grid-icon-state grid-icon-unaprove"><i class="fa fa-times"></i></div>
                                            </div>

                                            <div class="aurel-img-preloader"></div>
                                        </div>
                                    </div>

                                    <!-- Item 19 -->
                                    <div class="grid-item grid-item-proofing element grid_b2p approved">
                                        <div class="grid-item-inner">
                                            <div class="aurel_proofing_grid_item">
                                                <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/thumb/thumb-19.jpg" class="grid_thmb" alt=""/>

                                                <div class="grid-item-content">
                                                    <h4>Waterfall</h4>
                                                </div>

                                                <div class="grid-item-overlay aurel_js_bg_color" data-bgcolor="rgba(17,17,17,0.5)"></div>

                                                <div class="grid-item-controls">
                                                    <a class="aurel_pswp_slide grid-item-button-zoom" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-19.jpg" data-size="1200x1400" data-count="18"><i class="fa fa-search"></i></a>
                                                    <a class="grid-item-button-download" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-19.jpg" download><i class="fa fa-download"></i></a>
                                                    <a class="grid-item-button-unapprove" href="javascript:void(0)" data-moduleid="3368" data-uniqid="019"><i class="fa fa-times-circle"></i></a>
                                                    <a class="grid-item-button-approve" href="javascript:void(0)" data-moduleid="3368" data-uniqid="019"><i class="fa fa-minus-circle"></i></a>
                                                </div>

                                                <div class="grid-icon-state grid-icon-approve"><i class=""></i></div>

                                                <div class="grid-icon-state grid-icon-unaprove"><i class="fa fa-times"></i></div>
                                            </div>

                                            <div class="aurel-img-preloader"></div>
                                        </div>
                                    </div>

                                    <!-- Item 20 -->
                                    <div class="grid-item grid-item-proofing element grid_b2p approved">
                                        <div class="grid-item-inner">
                                            <div class="aurel_proofing_grid_item">
                                                <img src="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/thumb/thumb-20.jpg" class="grid_thmb" alt=""/>

                                                <div class="grid-item-content">
                                                    <h4>Street Style</h4>
                                                </div>

                                                <div class="grid-item-overlay aurel_js_bg_color" data-bgcolor="rgba(17,17,17,0.5)"></div>

                                                <div class="grid-item-controls">
                                                    <a class="aurel_pswp_slide grid-item-button-zoom" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-20.jpg" data-size="1200x1200" data-count="19"><i class="fa fa-search"></i></a>
                                                    <a class="grid-item-button-download" href="http://pixel-mafia.com/demo/html-templates/aurel/img/clipart/proofing_gallery/img-20.jpg" download><i class="fa fa-download"></i></a>
                                                    <a class="grid-item-button-unapprove" href="javascript:void(0)" data-moduleid="3368" data-uniqid="020"><i class="fa fa-times-circle"></i></a>
                                                    <a class="grid-item-button-approve" href="javascript:void(0)" data-moduleid="3368" data-uniqid="020"><i class="fa fa-minus-circle"></i></a>
                                                </div>

                                                <div class="grid-icon-state grid-icon-approve"><i class=""></i></div>

                                                <div class="grid-icon-state grid-icon-unaprove"><i class="fa fa-times"></i></div>
                                            </div>

                                            <div class="aurel-img-preloader"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="aurel_photo_proofing_notified_wrapper aurel_sm_mb_25">
                                    <a href="javascript:void(0)" class="aurel_button aurel_photo_proofing_notified">Notify Photographer</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection