<tr>
    <td>{{++$key}}</td>
    <td>{{ str_limit($tag->name, 47) }}</td>
    <td class="text-center">
        @if ($tag->is_published == '1' )
            <span class="label label-success">{{ $tag->is_published ? 'Yes' : 'No' }}</span>
        @elseif( $tag->is_published  == '0' )
            <span class="label label-warning">{{ $tag->is_published ? 'Yes' : 'No' }}</span>
        @endif
    </td>
    <td class="text-right">
        <button type="button" data-url="{{ route('tag.destroy', $tag->slug) }}" class="btn btn-flat btn-primary btn-xs item-delete">
            Delete
        </button>
        {{--<a href="{{route('tag.edit', $tag->slug)}}" class="btn btn-flat btn-primary btn-xs">--}}
            {{--Edit--}}
        {{--</a>--}}
        <button class="btn btn-flat btn-primary btn-xs" data-toggle="modal" data-target="#addTag" type="button">
            Edit
        </button>
    </td>
</tr>