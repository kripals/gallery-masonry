<div class="modal fade" id="addTag" tabindex="-1" role="dialog" aria-labelledby="addTagLabel">
    {{ Form::open(['route' => 'tag.store', 'class' => 'form']) }}
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="addTagLabel">Add Tag</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {{ Form::text('name', old('name'), ['class' => 'form-control']) }}
                    <label class="name">Name</label>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" name="draft" class="btn btn-info ink-reaction" value="Save Draft">
                <input type="submit" name="publish" class="btn btn-primary ink-reaction" value="Publish">
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>