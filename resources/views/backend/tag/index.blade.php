@extends('backend.layouts.app')

@section('title', 'Tag')

@push('styles')
@endpush

@section('content')
    <section>
        <div class="section-body">
            <div class="card">
                <div class="card-head">
                    <header class="text-capitalize">All Tags</header>
                    <div class="tools">
                        <button class="btn btn-primary ink-reaction" data-toggle="modal" data-target="#addTag" type="button">
                            <i class="md md-add"></i>
                            Add
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="60%">Name</th>
                            <th width="15%" class="text-center">Published</th>
                            <th class="text-right">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @each('backend.tag.partials.tables', $tag, 'tag')
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    @include('backend.tag.partials.addTagModal')
@stop
@push('scripts')
@endpush