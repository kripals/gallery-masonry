@extends('backend.layouts.app')

@section('title', 'Category')

@push('styles')
@endpush

@section('content')
    <section>
        <div class="section-body">
            <div class="card">
                <div class="card-head">
                    <header class="text-capitalize">All Category</header>
                    <div class="tools">
                        <button class="btn btn-primary ink-reaction" data-toggle="modal" data-target="#addCategory" type="button">
                            <i class="md md-add"></i>
                            Add
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="60%">Name</th>
                            <th width="15%" class="text-center">Published</th>
                            <th class="text-right">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @each('backend.category.partials.tables', $category, 'category')
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    @include('backend.category.partials.addCategoryModal')
@stop
@push('scripts')
@endpush