<tr>
    <td>{{++$key}}</td>
    <td>{{ str_limit($category->name, 47) }}</td>
    <td class="text-center">
        @if ($category->is_published == '1' )
            <span class="label label-success">{{ $category->is_published ? 'Yes' : 'No' }}</span>
        @elseif( $category->is_published  == '0' )
            <span class="label label-warning">{{ $category->is_published ? 'Yes' : 'No' }}</span>
        @endif
    </td>
    <td class="text-right">
        <button type="button" data-url="{{ route('category.destroy', $category->slug) }}" class="btn btn-flat btn-primary btn-xs item-delete">
            Delete
        </button>
        <a href="{{route('tag.edit', $category->slug)}}" class="btn btn-flat btn-primary btn-xs">
            Edit
        </a>
        {{--<button class="btn btn-flat btn-primary btn-xs" data-toggle="modal" data-target="#addTag" type="button">--}}
            {{--Edit--}}
        {{--</button>--}}
    </td>
</tr>