<section>
    <div class="section-body">
        {{ Form::model($tag, ['route' =>['tag.update', $tag->slug],'class'=>'form form-validate','role'=>'form', 'files'=>true, 'novalidate']) }}
        {{ method_field('PUT') }}
        @include('backend.tag.partials.addTagModal', ['header' => 'Edit Tag <span class="text-primary">('.str_limit($tag->name).')</span>'])
        {{ Form::close() }}
    </div>
</section>