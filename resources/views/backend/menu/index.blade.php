@extends('backend.layouts.app')

@section('title', 'Menu')

@push('styles')
@endpush

@section('content')
    <section>
        <div class="section-header text-center">
            <h2 class="style-default-bright">Menu</h2>
        </div>
        <div class="section-body">
            <div class="row">
                {{--@include('partials.errors')--}}
                {{ Form::open(['route' => 'menu.update', 'files' => true, 'method' => 'put', 'class' => 'form form-validation', 'novalidate']) }}
                <div class="col-md-8 col-md-offset-2">
                    <article class="margin-bottom-xxl">
                        <button class="btn btn-primary ink-reaction" data-toggle="modal" data-target="#addMenu"
                                type="button">
                            <i class="md md-add"></i>
                            Add
                        </button>
                        <button class="btn btn-primary ink-reaction" type="submit">
                            <i class="md md-save"></i>
                            Save
                        </button>
                    </article>
                </div>
                {{--<div class="col-md-12">--}}
                    {{--@foreach($menus as $menu)--}}
                        {{--<aside class="accordion">--}}
                            {{--<button type="button" class="btn btn-icon-toggle btn-add-sub-menu"--}}
                                    {{--data-url="{{ route('component.subMenuModal', $menu->slug) }}"--}}
                                    {{--data-toggle="tooltip" data-placement="top"--}}
                                    {{--data-original-title="Add Sub Menu"--}}
                                    {{--data-loading-text="<i class='fa fa-spinner fa-spin'></i>">--}}
                                {{--<i class="md md-add"></i>--}}
                            {{--</button>--}}
                            {{--@unless($menu->is_primary)--}}
                                {{--<a class="btn btn-icon-toggle btn-delete"--}}
                                   {{--data-url="{{ route('menu.destroy', $menu->slug) }}">--}}
                                    {{--<i class="md md-delete"></i>--}}
                                {{--</a>--}}
                            {{--@endunless--}}
                            {{--<h1>{{ $menu->name}}</h1>--}}
                            {{--<div style="padding-left: 20px">--}}
                                {{--@foreach($menu->subMenus as  $subMenu)--}}
                                    {{--<button type="button" class="btn btn-icon-toggle btn-add-child-sub-menu"--}}
                                            {{--data-url="{{ route('component.childsubMenuModal', $subMenu->slug) }}"--}}
                                            {{--data-toggle="tooltip" data-placement="top"--}}
                                            {{--data-original-title="Add Child Sub Menu"--}}
                                            {{--data-loading-text="<i class='fa fa-spinner fa-spin'></i>">--}}
                                        {{--<i class="md md-add"></i>--}}
                                    {{--</button>--}}
                                    {{--@unless($menu->is_primary)--}}
                                        {{--<a class="btn btn-icon-toggle btn-delete"--}}
                                           {{--data-url="{{ route('menu.subMenu.destroy',[$menu->slug, $subMenu->slug]) }}">--}}
                                            {{--<i class="md md-delete"></i>--}}
                                        {{--</a>--}}
                                    {{--@endunless--}}
                                    {{--<h2 style="padding-top: 15px; padding-bottom: 15px">{{ $subMenu->name }}</h2>--}}
                                    {{--<div class="opened-for-codepen" style="padding-left: 20px">--}}
                                        {{--@foreach( $subMenu->childsubMenus  as $childsubmenu)--}}
                                            {{--<h3 style="padding-top: 15px; padding-bottom: 15px">{{ $childsubmenu->name }}</h3>--}}
                                            {{--@unless($menu->is_primary)--}}
                                                {{--<a class="btn btn-icon-toggle btn-delete"--}}
                                                   {{--data-url="{{ route('menu.subMenu.childsubMenu.destroy',[$menu->slug, $subMenu->slug]) }}">--}}
                                                    {{--<i class="md md-delete"></i>--}}
                                                {{--</a>--}}
                                            {{--@endunless--}}
                                        {{--@endforeach--}}
                                    {{--</div>--}}
                                {{--@endforeach--}}
                            {{--</div>--}}
                        {{--</aside>--}}
                    {{--@endforeach--}}
                {{--</div>--}}
                {{ Form::close() }}
            </div>
        </div>
    </section>

    <div id="subMenuModal"></div>
    <div id="childsubMenuModal"></div>

    <div class="modal fade" id="addMenu" tabindex="-1" role="dialog" aria-labelledby="addMenuLabel">
        {{ Form::open(['route' => 'menu.store', 'class' => 'form']) }}
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="addMenuLabel">Add Menu</h4>
                </div>
                <div class="modal-body">
                    {{--<div class="form-group">--}}
                        {{--{{ Form::select('page', $pages, null, ['class' => 'form-control', 'placeholder' => 'Select a page or leave blank (#)']) }}--}}
                        {{--<label class="page">Page</label>--}}
                    {{--</div>--}}
                    <div class="form-group">
                        {{ Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '(same as page title)']) }}
                        <label class="name">Name</label>
                    </div>
                    <div class="form-group">
                        {{ Form::text('custom_url', old('custom_url'), ['class' => 'form-control', 'placeholder' => '(enter your custom URL here..)']) }}
                        <label class="name">Custom URL</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add Menu</button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@stop

@push('scripts')
<script>
    $(document).ready(function () {
//            $('.nestable-list').nestable();
        $(".btn-add-sub-menu").on("click", function (e) {
            e.stopPropagation();
            var $button = $(this);
            $.ajax({
                "type": "GET",
                "url": $button.data("url"),
                "beforeSend": function () {
                    $button.button('loading');
                },
                "complete": function () {
                    $button.button('reset');
                },
                "success": function (response) {
                    $("#subMenuModal").html(response);
                    $(document).find("#addSubMenu").modal();
                },
                "error": function () {
                    bootbox.alert("Error fetching modal!");
                }
            });
        });
        $(".btn-add-child-sub-menu").on("click", function (e) {
            e.stopPropagation();
            var $button = $(this);
            $.ajax({
                "type": "GET",
                "url": $button.data("url"),
                "beforeSend": function () {
                    $button.button('loading');
                },
                "complete": function () {
                    $button.button('reset');
                },
                "success": function (response) {
                    $("#childsubMenuModal").html(response);
                    $(document).find("#addChildSubMenu").modal();
                },
                "error": function () {
                    bootbox.alert("Error fetching modal!");
                }
            });
        });
        $(".btn-delete").on("click", function (e) {
            e.stopPropagation();
            var $button = $(this);
            bootbox.confirm("Are you sure?", function (response) {
                if (response)
                    $.ajax({
                        "type": "POST",
                        "url": $button.data("url"),
                        "data": {"_method": "DELETE"},
                        "success": function (response) {
                            if (response.Menu) {
                                $button.closest(".panel").detach();
                            } else {
                                $button.closest(".dd-item").detach();
                            }
                        },
                        "error": function () {
                            bootbox.alert("Error deleting menu!");
                        }
                    });
            });
        });
    });
</script>

<script src="{{ asset('backend/js/libs/jquery-validation/dist/jquery.validate.min.js') }}"></script>
<script src="{{ asset('backend/js/libs/jquery-validation/dist/additional-methods.min.js') }}"></script>
<script src="{{ asset('backend/js/libs/nestable/jquery.nestable.js') }}"></script>

@endpush