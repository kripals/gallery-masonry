@extends('backend.layouts.app')

@section('title', 'Wallpaper')

@push('styles')
@endpush

@section('content')
    <section>
        <div class="section-body">
            <div class="card">
                <div class="card-head">
                    <header class="text-capitalize">All Wallpapers</header>
                    <div class="tools">
                        <a class="btn btn-primary ink-reaction" href="{{ route('wallpaper.create') }}">
                            <i class="md md-add"></i>
                            Add
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="60%">Name</th>
                            <th width="15%" class="text-center">Published</th>
                            <th class="text-right">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @each('backend.wallpaper.partials.tables', $wallpaper, 'wallpaper')
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@stop
@push('scripts')
@endpush