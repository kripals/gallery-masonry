<tr>
    <td>{{++$key}}</td>
    <td>{{ str_limit($wallpaper->name, 47) }}</td>
    <td class="text-center">
        @if ($wallpaper->is_published == '1' )
            <span class="label label-success">{{ $wallpaper->is_published ? 'Yes' : 'No' }}</span>
        @elseif( $wallpaper->is_published  == '0' )
            <span class="label label-warning">{{ $wallpaper->is_published ? 'Yes' : 'No' }}</span>
        @endif
    </td>
    <td class="text-right">
        <button type="button" data-url="{{ route('wallpaper.destroy', $wallpaper->slug) }}" class="btn btn-flat btn-primary btn-xs item-delete">
            Delete
        </button>
        <a href="{{route('wallpaper.edit', $wallpaper->slug)}}" class="btn btn-flat btn-primary btn-xs">
            Edit
        </a>
    </td>
</tr>