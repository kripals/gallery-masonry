@extends('backend.layouts.app')

@section('title', 'Images')

@push('styles')
@endpush

@section('content')
    <section>
        <div class="section-body">
            <div class="card">
                <div class="card-head">
                    <header class="text-capitalize">All Images</header>
                    <div class="tools">
                        <a class="btn btn-primary ink-reaction" href="{{ route('img.create') }}">
                            <i class="md md-add"></i>
                            Add
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="60%">Name</th>
                            <th width="15%" class="text-center">Published</th>
                            <th class="text-right">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @each('backend.img.partials.tables', $img, 'img')
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@stop
@push('scripts')
@endpush