<tr>
    <td>{{++$key}}</td>
    <td>{{ str_limit($img->name, 47) }}</td>
    <td class="text-center">
        @if ($img->is_published == '1' )
            <span class="label label-success">{{ $img->is_published ? 'Yes' : 'No' }}</span>
        @elseif( $img->is_published  == '0' )
            <span class="label label-warning">{{ $img->is_published ? 'Yes' : 'No' }}</span>
        @endif
    </td>
    <td class="text-right">
        <button type="button" data-url="{{ route('img.destroy', $img->slug) }}" class="btn btn-flat btn-primary btn-xs item-delete">
            Delete
        </button>
        <a href="{{route('img.edit', $img->slug)}}" class="btn btn-flat btn-primary btn-xs">
            Edit
        </a>
    </td>
</tr>