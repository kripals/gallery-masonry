@extends('backend.layouts.app')

@section('title', 'Wallpaper')

@section('content')
    <section>
        <div class="section-body">
            {{ Form::model($wallpaper, ['route' =>['wallpaper.update', $wallpaper->slug],'class'=>'form form-validate','role'=>'form', 'files'=>true, 'novalidate']) }}
            {{ method_field('PUT') }}
            @include('backend.wallpaper.partials.form', ['header' => 'Edit Wallpaper <span class="text-primary">('.str_limit($wallpaper->name).')</span>'])
            {{ Form::close() }}
        </div>
    </section>
@stop

@push('styles')
<link href="{{ asset('backend/css/libs/dropify/dropify.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
<script src="{{ asset('backend/js/libs/jquery-validation/dist/jquery.validate.min.js') }}"></script>
<script src="{{ asset('backend/js/libs/jquery-validation/dist/additional-methods.min.js') }}"></script>
<script src="{{ asset('backend/js/libs/dropify/dropify.min.js') }}"></script>
@endpush