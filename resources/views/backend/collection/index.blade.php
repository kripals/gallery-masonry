@extends('backend.layouts.app')

@section('title', 'Collection')

@push('styles')
@endpush

@section('content')
    <section>
        <div class="section-body">
            <div class="card">
                <div class="card-head">
                    <header class="text-capitalize">All Collections</header>
                    <div class="tools">
                        <a class="btn btn-primary ink-reaction" href="{{ route('collection.create') }}">
                            <i class="md md-add"></i>
                            Add
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="60%">Name</th>
                            <th width="15%" class="text-center">Published</th>
                            <th class="text-right">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @each('backend.collection.partials.tables', $collection, 'collection')
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@stop
@push('scripts')
@endpush