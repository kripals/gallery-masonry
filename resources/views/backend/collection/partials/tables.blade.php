<tr>
    <td>{{++$key}}</td>
    <td>{{ str_limit($collection->name, 47) }}</td>
    <td class="text-center">
        @if ($collection->is_published == '1' )
            <span class="label label-success">{{ $collection->is_published ? 'Yes' : 'No' }}</span>
        @elseif( $collection->is_published  == '0' )
            <span class="label label-warning">{{ $collection->is_published ? 'Yes' : 'No' }}</span>
        @endif
    </td>
    <td class="text-right">
        <button type="button" data-url="{{ route('collection.destroy', $collection->slug) }}" class="btn btn-flat btn-primary btn-xs item-delete">
            Delete
        </button>
        <a href="{{route('collection.edit', $collection->slug)}}" class="btn btn-flat btn-primary btn-xs">
            Edit
        </a>
    </td>
</tr>