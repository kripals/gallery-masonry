@extends('backend.layouts.app')

@section('title', 'Collection')

@push('styles')
    <link href="{{ asset('backend/css/libs/dropify/dropify.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/libs/select2/select2.css') }}" rel="stylesheet">
@endpush

@section('content')
    <section>
        <div class="section-body">
            {{ Form::open(['route' =>'collection.store','class'=>'form form-validate','role'=>'form', 'files'=>true, 'novalidate']) }}
            @include('backend.collection.partials.form', ['header' => 'Create a Collection'])
            {{ Form::close() }}
        </div>
    </section>
@stop



@push('scripts')
    <script src="{{ asset('js/libs/select2/select2.min.js') }}"></script>
    <script src="{{ asset('backend/js/libs/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('backend/js/libs/jquery-validation/dist/additional-methods.min.js') }}"></script>
    <script src="{{ asset('backend/js/libs/dropify/dropify.min.js') }}"></script>
@endpush