<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.homepage');
});

Route::get('/', 'FrontendController@homepage')->name('homepage');
Route::get('/collections', 'FrontendController@collections')->name('collections');
Route::get('/wallpapers', 'FrontendController@wallpapers')->name('wallpapers');

Auth::routes();

/*
|--------------------------------------------------------------------------
| Backend routes
|--------------------------------------------------------------------------
*/
Route::group([ 'prefix' => 'backend', 'middleware' => 'auth' ], function () {
    Route::get('/home', 'HomeController@index')->name('home');

    /*
  |--------------------------------------------------------------------------
  | Settings Routes
  |--------------------------------------------------------------------------
  */
    Route::group([ 'as' => 'setting.', 'prefix' => 'setting' ], function () {
        Route::get('', 'SettingController@index')->name('index');
        Route::put('update', 'SettingController@update')->name('update');
    });

    /*
   |--------------------------------------------------------------------------
   | Menu CRUD Routes
   |--------------------------------------------------------------------------
   */
    Route::group([ 'as' => 'wallpaper.', 'prefix' => 'wallpaper' ], function () {
        Route::get('', 'WallpaperController@index')->name('index');
        Route::get('create-wallpaper', 'WallpaperController@create')->name('create');
        Route::post('', 'WallpaperController@store')->name('store');
        Route::get('{wallpaper}/edit', 'WallpaperController@edit')->name('edit');
        Route::put('{wallpaper}', 'WallpaperController@update')->name('update');
        Route::delete('{wallpaper}', 'WallpaperController@destroy')->name('destroy');
    });

    Route::group([ 'as' => 'collection.', 'prefix' => 'collection' ], function () {
        Route::get('', 'CollectionController@index')->name('index');
        Route::get('create-collection', 'CollectionController@create')->name('create');
        Route::post('', 'CollectionController@store')->name('store');
        Route::get('{collection}/edit', 'CollectionController@edit')->name('edit');
        Route::put('{collection}', 'CollectionController@update')->name('update');
        Route::delete('{collection}', 'CollectionController@destroy')->name('destroy');
    });

    Route::group([ 'as' => 'tag.', 'prefix' => 'tag' ], function () {
        Route::get('', 'TagController@index')->name('index');
        Route::post('', 'TagController@store')->name('store');
        Route::get('{tag}/edit', 'TagController@edit')->name('edit');
        Route::put('{tag}', 'TagController@update')->name('update');
        Route::delete('{tag}', 'TagController@destroy')->name('destroy');
    });

    Route::group([ 'as' => 'category.', 'prefix' => 'category' ], function () {
        Route::get('', 'CategoryController@index')->name('index');
        Route::post('', 'CategoryController@store')->name('store');
        Route::get('{category}/edit', 'CategoryController@edit')->name('edit');
        Route::put('{category}', 'CategoryController@update')->name('update');
        Route::delete('{category}', 'CategoryController@destroy')->name('destroy');
    });

    Route::group([ 'as' => 'img.', 'prefix' => 'image' ], function () {
        Route::get('', 'ImgController@index')->name('index');
        Route::get('create-img', 'ImgController@create')->name('create');
        Route::post('', 'ImgController@store')->name('store');
        Route::get('{img}/edit', 'ImgController@edit')->name('edit');
        Route::put('{img}', 'ImgController@update')->name('update');
        Route::delete('{img}', 'ImgController@destroy')->name('destroy');
    });

});