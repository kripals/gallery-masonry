<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Image Paths
    |--------------------------------------------------------------------------
    |
    | These values determine the image upload location for various models.
    | Paths should be finished with '/'
    |
    */

    'image' => [
        'App\Models\Wallpaper'  => 'images/wallpapers/',
        'App\Models\Collection' => 'images/collections/',
        'App\Models\Img'        => 'images/imgs/',

    ],

    /*
    |--------------------------------------------------------------------------
    | Document Paths
    |--------------------------------------------------------------------------
    |
    | These values determine the document upload location for various models.
    | Paths should be finished with '/'
    |
    */

    'icon' => [

        'User' => 'documents/'

    ],

    /*
    |--------------------------------------------------------------------------
    | Placeholder Paths
    |--------------------------------------------------------------------------
    |
    | These values determine the placeholder paths for various types.
    | Paths should be preceded and finished with '/'
    |
    */

    'placeholder' => [

        'default' => 'img/placeholder.jpg',
        'avatar'  => 'img/avatar_user.png'

    ]
];