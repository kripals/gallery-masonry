<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name'     => 'Gallery_admin',
            'email'    => 'admin@gallery.com.np',
            'password' => bcrypt('admin@gallery')
        ]);
    }
}
