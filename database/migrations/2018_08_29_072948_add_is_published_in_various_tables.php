<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsPublishedInVariousTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tables = [
            'wallpapers',
            'collections',
            'categories',
            'tags'
        ];

        foreach ($tables as $table)
        {
            Schema::table($table, function ($table)
            {
                $table->boolean('is_published')->default(0)->after('name');


            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
