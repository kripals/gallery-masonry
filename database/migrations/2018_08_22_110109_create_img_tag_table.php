<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImgTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('img_tag', function (Blueprint $table) {
            $table->engine   = 'InnoDB';
            $table->increments('id');
            $table->integer('img_id')->unsigned()->index();
            $table->integer('tag_id')->unsigned()->index();
            $table->foreign('img_id')
                ->references('id')
                ->on('imgs')
                ->onDelete('cascade');
            $table->foreign('tag_id')
                ->references('id')
                ->on('tags')
                ->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('img_tags');
    }
}
